package SimpleFraction;

public class Fraction {
	private double numerator, denominator;
	
	
	public Fraction(double numerator, double denominator) {
		
		this.numerator = numerator;
		this.denominator = denominator;
	}

	public Fraction() {
		this(0,0);
	}

	public String toString() {
			return "numerator :" +Double.valueOf(numerator).toString()+" denominator : "+Double.valueOf(denominator).toString();
					
		}


}
